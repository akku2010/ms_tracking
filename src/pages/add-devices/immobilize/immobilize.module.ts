import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImmobilizePage } from './immobilize';

@NgModule({
  declarations: [
    ImmobilizePage,
  ],
  imports: [
    IonicPageModule.forChild(ImmobilizePage)
  ]
})
export class ImmobilizePageModule {}