import { Component, OnInit } from "@angular/core";
import { ApiServiceProvider } from "../../../../providers/api-service/api-service";
import { ViewController, NavParams, ToastController, AlertController, IonicPage } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import * as moment from 'moment';

@IonicPage()
@Component({
    selector: 'page-update-cust',
    templateUrl: './update-cust.html'
})

export class UpdateCustModalPage implements OnInit {
    getAllDealersData: any;
    dealerdata: any;
    isSuperAdminStatus: boolean;
    updatecustForm: FormGroup;
    islogin: any;
    submitAttempt: boolean;
    devicedetail: any = {};
    customer: any;
    vehType: any;
    editdata: any;
    yearLater: any;
    minDate: any;

    constructor(
        public apiCall: ApiServiceProvider,
        public viewCtrl: ViewController,
        public formBuilder: FormBuilder,
        navPar: NavParams,
        public toastCtrl: ToastController,
        public alerCtrl: AlertController
    ) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // console.log("_id=> " + this.islogin._id);
        this.isSuperAdminStatus = this.islogin.isSuperAdmin;

        this.customer = navPar.get("param");
        console.log("customer details=> " + JSON.stringify(this.customer))
        // console.log("cust stat=> ", moment(this.customer.date, 'dd/mm/yyyy').toDate().toISOString());
        // console.log("one year later date=> " + this.customer.date)

        // if (this.customer.date == null) {
        //     var tempdate = new Date();
        //     tempdate.setDate(tempdate.getDate() + 365);
        //     // console.log("current year=> ", new Date(tempdate).toISOString())
        //     this.yearLater = moment(new Date(tempdate), 'DD-MM-YYYY').format('YYYY-MM-DD');
        //     // console.log("current year if=> ", this.yearLater)
        // } else {

        //     this.yearLater = moment(this.customer.date, 'DD/MM/YYYY').format('YYYY-MM-DD');
        //     // console.log("current year else=> ", this.yearLater)
        // }
        // debugger
        if (this.customer.expiration_date == null) {
            // console.log("creation date=> ", this.customer.date)
            // debugger;
            // var tru = moment(this.customer.created_on, 'DD/MM/YYYY').format('YYYY-MM-DD');
            // var tempdate = new Date(tru);
            // // console.log("type date=> ", tempdate)
            // tempdate.setDate(tempdate.getDate() + 365);
            // // console.log("current year=> ", new Date(tempdate).toISOString())
            // this.yearLater = moment(new Date(tempdate), 'DD-MM-YYYY').format('YYYY-MM-DD');
            var tru = moment(new Date(this.customer.created_on), 'DD/MM/YYYY').format('YYYY-MM-DD');
            var tempdate = new Date(tru);
            // console.log("type date=> ", tempdate)
            tempdate.setDate(tempdate.getDate() + 365);
            // console.log("current year=> ", new Date(tempdate).toISOString())
            this.yearLater = moment(new Date(tempdate), 'DD-MM-YYYY').format('YYYY-MM-DD');
            // console.log("current year if=> ", this.yearLater)
        } else {

            this.yearLater = moment(new Date(this.customer.expiration_date), 'DD/MM/YYYY').format('YYYY-MM-DD');
            // this.yearLater = this.customer.expiration_date;
            // console.log("current year else=> ", this.yearLater)
        }

        // ============== one month later date from current date ================
        var tdate = new Date();
        var eightMonthsFromJan312009 = tdate.setMonth(tdate.getMonth() + 1);
        this.minDate = moment(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log("one month later date=> " + moment(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD"))
        console.log("edited date=> ", moment(new Date(this.customer.created_on), 'DD/MM/YYYY').format('YYYY-MM-DD'));
        console.log("expiration date=> ", this.yearLater)
        // =============== end
        this.updatecustForm = formBuilder.group({
            userid: [this.customer.userid, Validators.required],
            first_name: [this.customer.first_name, Validators.required],
            last_name: [this.customer.last_name, Validators.required],
            email: [this.customer.email, Validators.required],
            phone: [this.customer.phone, Validators.required],
            address: [this.customer.address, Validators.required],
            creationdate: [moment(new Date(this.customer.created_on), 'DD/MM/YYYY').format('YYYY-MM-DD')],
            expirationdate: [this.yearLater],
            dealer_firstname: [this.customer.dealer_firstname],
        })

    }

    ngOnInit() {
        this.getAllDealers();
    }

    getAllDealers() {
        console.log("get user")
        var baseURLp = 'https://www.oneqlik.in/users/getAllDealerVehicles';
        this.apiCall.getAllDealerVehiclesCall(baseURLp)
            .subscribe(data => {
                this.getAllDealersData = data;
            }, err => {
                console.log(err);
            });
    }

    DealerselectData(dealerselect) {
        console.log(dealerselect);
        this.dealerdata = dealerselect;
        console.log(this.dealerdata.dealer_id);
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    updateCustomer() {
        this.submitAttempt = true;
        if (this.updatecustForm.valid) {
            console.log(this.updatecustForm.value);
            this.devicedetail = {
                "contactid": this.customer._id,
                "address": this.updatecustForm.value.address,
                "expire_date": new Date(this.updatecustForm.value.expirationdate).toISOString(),
                "first_name": this.updatecustForm.value.first_name,
                "last_name": this.updatecustForm.value.last_name,
                "status": this.customer.status,
                "user_id": this.updatecustForm.value.userid
            }

            if (this.vehType == undefined) {
                this.devicedetail;
            } else {
                this.devicedetail.vehicleType = this.vehType._id;
                // this.vehType._id
            }
            console.log(this.devicedetail);

            this.apiCall.startLoading().present();
            this.apiCall.editUserDetailsCall(this.devicedetail)
                .subscribe(data => {
                    this.apiCall.stopLoading();
                    this.editdata = data;
                    // console.log("editdata=>"+ this.editdata);

                    let toast = this.toastCtrl.create({
                        message: "data updated successfully!",
                        position: 'bottom',
                        duration: 2000
                    });

                    toast.onDidDismiss(() => {
                        console.log('Dismissed toast');
                        this.viewCtrl.dismiss(this.editdata);
                    });

                    toast.present();
                }, err => {
                    this.apiCall.stopLoading();
                    var body = err._body;
                    var msg = JSON.parse(body);
                    let alert = this.alerCtrl.create({
                        title: 'Oops!',
                        message: msg.message,
                        buttons: ['OK']
                    });
                    alert.present();
                });
        }

    }
}