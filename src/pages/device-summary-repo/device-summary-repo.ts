import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
declare var google;
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-device-summary-repo',
  templateUrl: 'device-summary-repo.html',
})
export class DeviceSummaryRepoPage implements OnInit {

  islogin: any;
  devices: any;
  portstemp: any;
  datetimeStart: string;
  datetimeEnd: string;
  device_id: any;
  summaryReport: any[] = [];
  locationEndAddress: any;
  locationAddress: any;
  datetime: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apicallsummary: ApiServiceProvider, public toastCtrl: ToastController) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    // var yestDay = moment().subtract(1, 'days');
    // console.log("yesterdays date: ", yestDay); 
    console.log("yest time: ", moment({ hours: 0 }).subtract(1, 'days').format())
    // this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeStart = moment({ hours: 0 }).subtract(1, 'days').format(); // yesterday date with 12:00 am
    console.log("today time: ", this.datetimeStart)
    // this.datetimeEnd = moment().format();//new Date(a).toISOString();
    this.datetimeEnd = moment({ hours: 0 }).format(); // today date and time with 12:00am
  }

  ngOnInit() {
    this.getdevices();
  }

  getSummaarydevice(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.device_id = selectedVehicle.Device_ID;
  }

  getdevices() {
    var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicallsummary.startLoading().present();
    this.apicallsummary.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicallsummary.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apicallsummary.stopLoading();
          console.log(err);
        });
  }

  summaryReportData = [];
  getSummaryReport() {
    let that = this;
    this.summaryReport = []
    this.summaryReportData = [];

    if (this.device_id == undefined) {
      this.device_id = "";

    }
    this.apicallsummary.startLoading().present();
    this.apicallsummary.getSummaryReportApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.islogin._id, this.device_id)
      .subscribe(data => {
        this.apicallsummary.stopLoading();
        this.summaryReport = data;
        if (this.summaryReport.length > 0) {
          this.innerFunc(this.summaryReport);
        } else {
          let toast = this.toastCtrl.create({
            message: 'Report(s) not found for selected dates/vehicle.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }
      }, error => {
        this.apicallsummary.stopLoading();
        console.log(error);
      })
  }

  innerFunc(summaryReport) {
    let outerthis = this;
    var i = 0, howManyTimes = summaryReport.length;
    function f() {
      outerthis.summaryReportData.push(
        {
          'Device_Name': outerthis.summaryReport[i].devObj[0].Device_Name,
          'routeViolations': outerthis.summaryReport[i].today_routeViolations,
          'overspeeds': outerthis.summaryReport[i].today_overspeeds,
          'ignOn': outerthis.summaryReport[i].today_running,
          'ignOff': outerthis.summaryReport[i].today_stopped,
          'distance': outerthis.summaryReport[i].total_odo,
          'tripCount': outerthis.summaryReport[i].today_trips
        });

      if (outerthis.summaryReport[i].end_location != null && outerthis.summaryReport[i].start_location != null) {
        var latEnd = outerthis.summaryReport[i].end_location.lat;
        var lngEnd = outerthis.summaryReport[i].end_location.long;
        var latlng = new google.maps.LatLng(latEnd, lngEnd);

        var latStart = outerthis.summaryReport[i].start_location.lat;
        var lngStart = outerthis.summaryReport[i].start_location.long;


        var lngStart1 = new google.maps.LatLng(latStart, lngStart);
        var geocoder = new google.maps.Geocoder();

        var request = {
          latLng: latlng
        };

        var request1 = {
          latLng: lngStart1
        };

        geocoder.geocode(request, function (data, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (data[1] != null) {
              outerthis.locationEndAddress = data[1].formatted_address;

            }
          }
          outerthis.summaryReportData[outerthis.summaryReportData.length - 1].EndLocation = outerthis.locationEndAddress;
        })

        geocoder.geocode(request1, function (data, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (data[1] != null) {
              outerthis.locationAddress = data[1].formatted_address;
            }
          }
          outerthis.summaryReportData[outerthis.summaryReportData.length - 1].StartLocation = outerthis.locationAddress;
        })
      } else {
        outerthis.summaryReportData[outerthis.summaryReportData.length - 1].EndLocation = 'N/A';
        outerthis.summaryReportData[outerthis.summaryReportData.length - 1].StartLocation = 'N/A';
      }
      console.log(outerthis.summaryReportData);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 5000);
      }
    }
    f();
  }
}
