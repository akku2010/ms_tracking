import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DistanceReportPage } from './distance-report';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    DistanceReportPage,
  ],
  imports: [
    IonicPageModule.forChild(DistanceReportPage),
    SelectSearchableModule
  ],
})
export class DistanceReportPageModule {}
