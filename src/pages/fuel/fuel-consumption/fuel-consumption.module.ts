import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuelConsumptionPage } from './fuel-consumption';
import { IonPullupModule } from 'ionic-pullup';

@NgModule({
  declarations: [
    FuelConsumptionPage,
  ],
  imports: [
    IonicPageModule.forChild(FuelConsumptionPage),
    IonPullupModule
  ],
})
export class FuelConsumptionPageModule {}
