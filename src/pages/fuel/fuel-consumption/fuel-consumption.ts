import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IonPullUpFooterState } from 'ionic-pullup';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-fuel-consumption',
  templateUrl: 'fuel-consumption.html',
})
export class FuelConsumptionPage implements OnInit {
  footerState: IonPullUpFooterState;
  islogin: any;
  portstemp: any;
  constructor(public apiCall: ApiServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FuelConsumptionPage');
  }
  ngOnInit() {
    this.getdevices();
  }
  toggleFooter() {
    this.footerState = this.footerState == IonPullUpFooterState.Collapsed ? IonPullUpFooterState.Expanded : IonPullUpFooterState.Collapsed;
  }

  getdevices() {
    var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }

    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.portstemp = data.devices;
      },
        err => {
          // this.apicalldaily.stopLoading();
          console.log(err);
        });
  }

  onChnageEvent(veh) {
    console.log("vehicle info:", veh)
  }

  submit() {

  }

  footerExpanded() {
    console.log('Footer expanded!');
  }

  footerCollapsed() {
    console.log('Footer collapsed!');
  }

}
