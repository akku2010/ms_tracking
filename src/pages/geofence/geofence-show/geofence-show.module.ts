import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeofenceShowPage } from './geofence-show';

@NgModule({
  declarations: [
    GeofenceShowPage,
  ],
  imports: [
    IonicPageModule.forChild(GeofenceShowPage),
  ],
})
export class GeofenceShowPageModule {}