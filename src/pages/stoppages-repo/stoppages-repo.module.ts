import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StoppagesRepoPage } from './stoppages-repo';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    StoppagesRepoPage,
  ],
  imports: [
    IonicPageModule.forChild(StoppagesRepoPage),
    SelectSearchableModule
  ],
})
export class StoppagesRepoPageModule {}
